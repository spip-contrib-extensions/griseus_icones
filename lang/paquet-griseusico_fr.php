<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'griseusico_nom' => 'Griseus : icônes espace privé',
	'griseusico_slogan' => 'Un jeu d\'icônes pour l\'espace privé',
	'griseusico_description' => 'Ce plugin fait partie du thème "Griseus". Il remplace les icônes de l\'espace privé par un jeu d\'icônes qui se veut sobre et tente de faire en sorte de ne pas détourner l\'attention du contenu.'
);

?>